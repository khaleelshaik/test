package com.accenture.fhir.Test;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
	
	public void test1() {
		System.out.println( "Test 1" );
	}
    
	public void test2() {
		System.out.println( "Test 2" );
	}

	public void test() {
    	int a = 2;
    	int b = 3;
    	int c = a + b;
    	if(3 == c) {
    		System.out.println("Equals");
    	} else {
    		System.out.println("Not Equals");
    	}
    	
    	String s = "";
    	if(s == null) {
    		System.out.println("s is null");
    	}
    }
}
